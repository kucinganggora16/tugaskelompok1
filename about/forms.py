from django import forms
from .models import questionModel
from django.forms import ModelForm

class questionForm(forms.ModelForm):

    class Meta:
        model = questionModel
        fields = ['name', 'email', 'message']
        labels = {'name': 'Nama', 'email': 'E-mail', 'message': 'Pertanyaan'}
        widgets={
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'message': forms.Textarea(attrs={'class': 'form-control', 'rows':'4'})
        }