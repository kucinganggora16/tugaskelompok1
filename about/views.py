from django.shortcuts import render, redirect
from .forms import questionForm
from .models import questionModel
from django.contrib import messages
from django.http import HttpResponseRedirect

def about(request):
    form=questionForm()        
    database = questionModel.objects.all()
    data = {'form':form, 'database': database}
    return render(request, 'about/about.html', data)

def question(request):
    
    if request.method == 'POST':
        form = questionForm(request.POST)
        if form.is_valid():
            fields=['name','email','message']
            form.save()

    return HttpResponseRedirect("/about")