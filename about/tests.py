from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import about, question
from .models import questionModel
from .forms import questionForm

# menonton, mengikuti:
# https://www.youtube.com/watch?v=0MrgsYswT1c&list=PLbpAWbHbi5rMF2j5n6imm0enrSD9eQUaM&index=2
# https://gitlab.com/PPW-2017/ppw-lab

class TestAbout(TestCase):

    def setUp(self):
        self.client = Client()
        self.data = {'name':'dinda', 'email':'dinda.ip@gmail.com', 'message':'mau nanya dong'}

    def test_about_url_is_exist(self):
        # mengecek apakah urlnya jalan atau tidak
        response = self.client.get('/')
        self.assertEqual(response.status_code,200)

    def test_about_use_about_func(self):
        # mengecek apakah about/ memanggil fungsi about atau tidak
        url=resolve('/about/')
        self.assertEqual(url.func, about)

    def test_daftar_use_about_func(self):
        # mengecek apakah about/daftar memanggil fungsi question atau tidak
        url=resolve('/about/daftar/')
        self.assertEqual(url.func, question)

    def test_about_use_about_template(self):
        # mengecek apakah about/ menggunakan template about.html yang
        # ada di dalam folder about
        response = self.client.get('/about/')
        self.assertTemplateUsed(response, 'about/about.html')

    def test_question_able_to_save_POST_request(self):
        # fungsi question berhasil menyimpan post yang valid
        # dan /daftar memang terhubung dengan fungsi question
        # dan question redirect kembali ke /about
        response=self.client.post('/about/daftar/', self.data)
        count_object_yang_dibuat = questionModel.objects.all().count()
        self.assertEqual(count_object_yang_dibuat,1)

        self.assertEqual(response.status_code, 302) # 302 berarti redirect
        self.assertEqual(response['location'], '/about')

        html_resp=self.client.get('/about/').content.decode('utf8')
        self.assertIn('mau nanya dong', html_resp)

    def test_question_post_error(self):
        # mengecek bila masukan postnya '' dan melalui isvalid
        # lalu mencari variabel di data yang kosong tersebut ('' semua)
        # maka variabel tersebut tidak ditemukan
        nama = 'dinda'
        response_post= self.client.post('/about/daftar/', {'name':'', 'email':'', 'message':''})
        self.assertEqual(response_post.status_code, 302)

        html_response=response=self.client.get('/about/').content.decode('utf8')
        self.assertNotIn(nama,html_response)

    def test_question_model_can_create_new_question(self):
        # mengecek apakah questionModel dapat ditambahkan data
        new_question=questionModel.objects.create(name='dinda', email='dinda.ip@gmail.com', message='mau nanya dong')

        count_object_yang_dibuat = questionModel.objects.all().count()
        self.assertEqual(count_object_yang_dibuat,1)
    
    def test_form_valid(self):
        # mengecek apakah jika isi form valid maka is_valid() true
        form = questionForm(self.data)
        self.assertTrue(form.is_valid())

    def test_form_no_data(self):
        # mengecek apakah form tidak ada isinya maka is_valid() false
        form = questionForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['message'], ["This field is required."])

    def test_show_all_questions(self):
        # mengecek semua isi database muncul di about/
        data_inas = {'name':'inas', 'email':'dinda.inas@yahoo.com', 'message':'boleh nanya ngga'}

        post_data_dinda = self.client.post('/about/daftar/', self.data)
        post_data_inas = self.client.post('/about/daftar/', data_inas)

        self.assertEqual(post_data_dinda.status_code, 302)
        self.assertEqual(post_data_inas.status_code, 302)

        html_response = self.client.get('/about/').content.decode('utf8')
        
        self.assertIn('dinda', html_response)
        self.assertIn('mau nanya dong', html_response)

        self.assertIn('inas', html_response)
        self.assertIn('boleh nanya ngga', html_response)