from django.shortcuts import render
from frontpage.forms import UserForm

# Create your views here.
def frontpage(request):
    return render(request, 'frontpage/frontpage.html')

def register(request):
    konten = {'form'  : UserForm}
    

    if request.method == "POST":
        user_form = UserForm(data= request.POST)

        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

    return render(request, 'frontpage/register.html', konten)