from django.urls import path, include
from frontpage import views

app_name = 'frontpage'

urlpatterns = [
    path('', views.frontpage, name="beranda"),
    path('login/', views.register, name= "register")
]