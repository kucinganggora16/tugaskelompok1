from django.test import TestCase, Client
from django.http import HttpRequest


from frontpage.views import frontpage
from frontpage.models import UserForm

# Create your tests here.
class FrontpageTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.data = {'username':'sss', 'email':'sss@gmail.com', 'password':'Ssssss1!'}

    def test_frontpage_url_exist(self):
        client = Client()
        respose = client.get("/")

        self.assertEqual(respose.status_code, 200)
    
    def test_register_url_exist(self):
        client = Client()
        respose = client.get("/login")

        self.assertEqual(respose.status_code, 200)

    def test_question_able_to_save_POST_request(self):
        response=self.client.post('/login', self.data)
        
        muncul = UserForm.objects.all().count()
        self.assertEqual(muncul,1)

        self.assertEqual(response.status_code, 200)

    def test_form_valid(self):
        form = UserForm(self.data)
        self.assertTrue(form.is_valid())
    


