from django.urls import path, include
from laporan import views

app_name = 'laporan'

urlpatterns = [
    path('lihatLaporan/', views.lihatLaporan, name='lihatLaporan'),
    path('ubahLaporan/', views.ubahLaporan, name='ubahLaporan'),
    path('ubahLaporan/<int:id>', views.ubahStatusLaporan, name='ubahStatusLaporan'),
]