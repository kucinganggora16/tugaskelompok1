from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import reverse, resolve

from laporan.views import lihatLaporan, ubahLaporan
from laporan.models import PilihStatus
from laporan.forms import FormPilihStatus

# Create your tests here.
class LaporanTestCase(TestCase):
    def test_lihatLaporan_url_ada(self):
        client = Client()
        response = client.get(reverse('laporan:lihatLaporan'))
        self.assertEqual(response.status_code, 200)

    def test_ubahLaporan_url_ada(self):
        client = Client()
        response = client.get(reverse('laporan:ubahLaporan'))
        self.assertEqual(response.status_code, 200)

    def test_lihatLaporan_template_benar(self):
        response = self.client.get(reverse('laporan:lihatLaporan'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'laporan/lihat_laporan.html')

    def test_ubahLaporan_template_benar(self):
        response = self.client.get(reverse('laporan:ubahLaporan'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'laporan/ubah_laporan.html')

    def test_lihatLaporan_html_benar(self):
        response = self.client.get(reverse('laporan:lihatLaporan'))
        self.assertContains(response, '<div id="lihat_laporan">')

    def test_ubahLaporan_html_benar(self):
        response = self.client.get(reverse('laporan:ubahLaporan'))
        self.assertContains(response, '<div id="ubah_laporan">')

    def test_ubahLaporan_buat_status_berhasil(self):
        PilihStatus.objects.create(status='Ditolak')
        PilihStatus_count = PilihStatus.objects.all().count()
        self.assertEqual(PilihStatus_count, 1)

    def test_ubahLaporan_update_status_berhasil(self):
        obj = PilihStatus.objects.create(status="Diproses")
        PilihStatus.objects.filter(pk=obj.pk).update(status="Ditolak")
        obj.refresh_from_db()
        self.assertEqual(obj.status, "Ditolak")

    def test_ubahLaporan_status_kosong_ga_masuk_database(self):
        Client().post(reverse('laporan:ubahLaporan'), {'status': ''})
        PilihStatus_count = PilihStatus.objects.all().count()
        self.assertEqual(PilihStatus_count, 0)

    def test_lihatLaporan_page_pake_def_lihatLaporan(self):
        found = resolve(reverse('laporan:lihatLaporan'))
        self.assertEqual(found.func, lihatLaporan)

    def test_ubahLaporan_page_pake_def_ubahLaporan(self):
        found = resolve(reverse('laporan:ubahLaporan'))
        self.assertEqual(found.func, ubahLaporan)

    def test_FormPilihStatus_pake_css_class(self):
        form = FormPilihStatus()
        self.assertIn('class="form-control"', form.as_p())

    
