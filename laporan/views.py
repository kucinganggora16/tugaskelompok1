from django.shortcuts import render, redirect
from form.models import Lapor
from form.forms import BuatLaporan
from .forms import FormPilihStatus

# Create your views here.
def lihatLaporan(request):
    respon = Lapor.objects.all()
    konten = {'title' : 'Lihat Status Laporan', 'respon' : respon}
    return render(request, 'laporan/lihat_laporan.html', konten)

def ubahLaporan(request):
    respon = Lapor.objects.all()
    form = FormPilihStatus()
    konten = {'title' : 'Ubah Status Laporan', 'respon' : respon, 'lapor':form}
    return render(request, 'laporan/ubah_laporan.html', konten)

def ubahStatusLaporan(request, id):
    ubahStatus = Lapor.objects.get(pk=id)
    respon = Lapor.objects.all()
    form = FormPilihStatus(request.POST or None, instance=ubahStatus)
    if form.is_valid():
        ubahStatus = form.save(commit=False)
        ubahStatus.save()
        konten = {'title' : 'Ubah Status Laporan', 'form' : form, 'respon' : respon, 'ubahStatus' : ubahStatus}
        return redirect('/laporan/ubahLaporan')
    else:
        konten = {'title' : 'Ubah Status Laporan', 'form' : form, 'respon' : respon, 'ubahStatus' : ubahStatus}
        return redirect('/laporan/ubahLaporan')