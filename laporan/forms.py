from django import forms
from form.models import Lapor
from django.forms import ModelForm

class FormPilihStatus(ModelForm):
    class Meta:
        model = Lapor
        fields = ['status']
        labels = {'status' : ''}
        widgets = {'status' : forms.Select(attrs={'class' : 'form-control'})}