from django import forms
from .models import Lapor


class BuatLaporan(forms.ModelForm):
    class Meta():
        model = Lapor
        fields = ['nama', 'kontak', 'laporan']
        