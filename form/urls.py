from django.urls import path, include
from form import views

app_name = 'form'

urlpatterns = [
    path('', views.buatLaporan, name='buatLaporan'),
]