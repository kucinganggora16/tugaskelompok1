from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import reverse, resolve

from form.views import buatLaporan

# Create your tests here.
class FormTestCase(TestCase):

    def test_buatLaporan_url_exist(self):
        response = Client().get("/buatLaporan/")

        self.assertEqual(response.status_code, 200)
    
    def test_text_nama_exist(self):
        response = self.client.get('/buatLaporan/')
        isi_html = response.content.decode("utf8")
        self.assertIn("Nama", isi_html)

    def test_text_kontak_exist(self):
        response = self.client.get('/buatLaporan/')
        isi_html = response.content.decode("utf8")
        self.assertIn("Kontak", isi_html)
    
    def test_text_laporan_exist(self):
        response = self.client.get('/buatLaporan/')
        isi_html = response.content.decode("utf8")
        self.assertIn("Laporan", isi_html)
    
    def test_header_CeritakanMasalahmuDisini_exist(self):
        response = self.client.get('/buatLaporan/')
        isi_html = response.content.decode("utf8")
        self.assertIn("Ceritakan Masalahmu Disini", isi_html)

    def test_button_laporsis_exist(self):
        response = self.client.get('/buatLaporan/')
        isi_html = response.content.decode("utf8")
        self.assertIn("<input", isi_html)
        self.assertIn('type="submit"', isi_html)
        self.assertIn("Lapor-sis!", isi_html)
    
    def test_buatLaporan_template_benar(self):
        response = self.client.get(reverse('form:buatLaporan'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'form/buat_laporan.html')
    
    def test_buatLaporan_html_benar(self):
        response = self.client.get(reverse('form:buatLaporan'))
        self.assertContains(response, '<div id="buat_laporan">')