from django.shortcuts import render, redirect
from .forms import BuatLaporan
from .models import Lapor
from . import forms
from django.http import HttpResponseRedirect

# Create your views here.
def buatLaporan(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = BuatLaporan(request.POST or None)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            form = BuatLaporan()
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/buatLaporan/')
    
    # if a GET (or any other method) we'll create a blank form
    else:
        form = BuatLaporan()

    return render(request, 'form/buat_laporan.html', {'form':form})