from django.db import models

# Create your models here.
class Lapor(models.Model):
    nama = models.CharField(max_length=50)
    kontak = models.CharField(max_length=50)
    laporan = models.TextField()
    #bukti = models.FileField(upload_to=None)
    
    #Sambungin Models milik apps laporan
    status = models.ForeignKey('laporan.PilihStatus', on_delete=models.DO_NOTHING, default=1)
    

    def __str__(self):
        return self.nama
